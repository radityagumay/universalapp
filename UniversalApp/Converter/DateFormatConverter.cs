﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Globalization;
using Windows.Globalization.DateTimeFormatting;
using Windows.UI.Xaml.Data;

namespace UniversalApp.Converter
{
    public class DateFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                string val = String.Concat(value, "000");
                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime date = start.AddMilliseconds(double.Parse(val, System.Globalization.CultureInfo.InvariantCulture)).ToLocalTime();
                return ((DateTime)date).ToString("dd/MM/yy");
            }
            else
            {
                return String.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            string val = String.Concat(value, "000");
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(double.Parse(val, System.Globalization.CultureInfo.InvariantCulture)).ToLocalTime();
            return DateTime.Parse(date.ToString());
        }
    }
}
