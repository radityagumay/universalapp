﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UniversalApp.Serializes
{
    [DataContract]
    public class ApiServiceSerialize
    {
        public ApiServiceSerialize()
        {
            city = new City();
            list = new List<List>();
        }

        [DataMember(Name = "city")]
        public City city { get; set; }
        [DataMember(Name = "cod")]
        public string cod { get; set; }
        [DataMember(Name = "message")]
        public double message { get; set; }
        [DataMember(Name = "cnt")]
        public int cnt { get; set; }
        [DataMember(Name = "list")]
        public List<List> list { get; set; }
    }

    [DataContract]
    public class Coord
    {
        [DataMember(Name = "lon")]
        public double lon { get; set; }
        [DataMember(Name = "lat")]
        public double lat { get; set; }
    }

    [DataContract]
    public class City
    {
        public City()
        {
            coord = new Coord();
        }

        [DataMember(Name = "id")]
        public int id { get; set; }
        [DataMember(Name = "name")]
        public string name { get; set; }
        [DataMember(Name = "coord")]
        public Coord coord { get; set; }
        [DataMember(Name = "country")]
        public string country { get; set; }
        [DataMember(Name = "population")]
        public int population { get; set; }
    }

    [DataContract]
    public class Temp
    {
        [DataMember(Name = "day")]
        public double day { get; set; }
        [DataMember(Name = "min")]
        public double min { get; set; }
        [DataMember(Name = "max")]
        public double max { get; set; }
        [DataMember(Name = "night")]
        public double night { get; set; }
        [DataMember(Name = "eve")]
        public double eve { get; set; }
        [DataMember(Name = "morn")]
        public double morn { get; set; }
    }

    [DataContract]
    public class Weather
    {
        [DataMember(Name = "id")]
        public int id { get; set; }
        [DataMember(Name = "main")]
        public string main { get; set; }
        [DataMember(Name = "description")]
        public string description { get; set; }
        [DataMember(Name = "icon")]
        public string icon { get; set; }
    }

    [DataContract]
    public class List
    {
        public List()
        {
            temp = new Temp();
            weather = new List<Weather>();
        }

        [DataMember(Name = "dt")]
        public int dt { get; set; }
        [DataMember(Name = "temp")]
        public Temp temp { get; set; }
        [DataMember(Name = "pressure")]
        public double pressure { get; set; }
        [DataMember(Name = "humidity")]
        public int humidity { get; set; }
        [DataMember(Name = "weather")]
        public List<Weather> weather { get; set; }
        [DataMember(Name = "speed")]
        public double speed { get; set; }
        [DataMember(Name = "deg")]
        public int deg { get; set; }
        [DataMember(Name = "clouds")]
        public int clouds { get; set; }
        [DataMember(Name = "rain")]
        public double rain { get; set; }
    }
}
