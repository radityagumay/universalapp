﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UniversalApp.Serializes;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace UniversalApp.Services
{
    public class ApiService : IApiService
    {
        private const string foreCastUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
        private string urlRequest, address;

        private HttpBaseProtocolFilter filter;
        private HttpClient httpClient;

        public ApiService()
        {
            this.filter = new HttpBaseProtocolFilter();
            this.httpClient = new HttpClient(filter);
        }

        public async void onSearchByCity(string city, int days, Action<ApiServiceSerialize, Exception> callback)
        {
            urlRequest = String.Concat(foreCastUrl, "{0}&mode=json&cnt={1}&units=metric");
            address = String.Format(urlRequest, city, days);

            Uri resourceUri;
            HttpResponseMessage response = null;

            if (!Helpers.Helpers.TryGetUri(address, out resourceUri))
            {
                callback.Invoke(null, new Exception("Invalid Uri"));
                return;
            }

            try
            {
                response = await httpClient.GetAsync(resourceUri);
            }
            catch (Exception ex)
            {
                callback.Invoke(null, ex);
            }
            finally
            {
                if (response != null)
                {
                    ApiServiceSerialize pojo = null;
                    try
                    {
                        pojo = JsonConvert.DeserializeObject<ApiServiceSerialize>(response.Content.ToString());
                    }
                    catch (Exception ex)
                    {
                        callback.Invoke(null, new Exception("Invalid Pojo response"));
                    }
                    callback.Invoke(pojo, null);
                }
            }
        }
    }
}
