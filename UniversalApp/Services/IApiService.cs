﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalApp.Serializes;

namespace UniversalApp.Services
{
    public interface IApiService
    {
        void onSearchByCity(string city, int days, Action<ApiServiceSerialize, Exception> callback);
    }
}
