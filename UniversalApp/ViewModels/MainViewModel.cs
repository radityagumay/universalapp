﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using UniversalApp.Models;
using UniversalApp.Serializes;
using UniversalApp.Services;
using Windows.UI.Core;
using Windows.UI.Popups;

namespace UniversalApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region properties
        private ApiServiceSerialize service;
        public ApiServiceSerialize Service
        {
            get
            {
                return service;
            }

            set
            {
                service = value;
                NotifyPropertyChanged("Service");
            }
        }

        private string query;
        public string Query
        {
            get
            {
                return query;
            }

            set
            {
                query = value;
                NotifyPropertyChanged("Query");
            }
        }
        
        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (searchCommand == null)
                {
                    searchCommand = new Helpers.RelayCommandHelper(
                        param => this.OnCommand(),
                        param => this.CanSave());
                }
                return searchCommand;
            }

        }

        /**
        * Verify command can be executed here
        **/
        private bool CanSave()
        {
            Debug.WriteLine("RADITYA GUMAY CanSave");
            return true;
        }

        /**
        * Save command execution logic
        **/
        private void OnCommand()
        {
            if (!String.IsNullOrEmpty(query))
            {
                InitService(query);
                Debug.WriteLine("RADITYA GUMAY OnCommand");
            }
        }
        #endregion

        #region property
        #endregion
        public MainViewModel()
        {
            Service = new ApiServiceSerialize();
            InitService("");
        }

        private void InitService(string q)
        {
            ApiService servie = new ApiService();
            servie.onSearchByCity(String.IsNullOrEmpty(q) ? "Jakarta" : q, 16, (result, ex) =>
            {
                var dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;
                var ignored = dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    if (result == null) {
                        var dialog = new MessageDialog("Your message here");
                        await dialog.ShowAsync();
                    }
                    else 
                    initUI(result);
                });
            });
        }

        private void initUI(ApiServiceSerialize result)
        {
            for (int i = 0; i < result.list.Count; i++)
            {
                for (int j = 0; j < result.list[i].weather.Count; j++)
                {
                    string con = String.Concat("http://openweathermap.org/img/w/", "{0}{1}");
                    string iconUrl = String.Format(con, result.list[i].weather[j].icon, ".png");
                    result.list[i].weather[j].icon = iconUrl;
                }
            }

            service.city = result.city;
            service.list = result.list.ToList();
            NotifyPropertyChanged("Service");
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
